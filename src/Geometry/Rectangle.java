package Geometry;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Rectangle extends Figure  {
    private double a;
    private double b;

    private Rectangle( Color color, int lineUidth, Point center) {
        super(FIGURE_TYPE_RECTANGLE, color, lineUidth, center);
    }

    public Rectangle(Color color, int lineUidth, Point center, double a, double b) {
       this(color, lineUidth, center);
        this.a = a;
        this.b = b;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle rectangle = (Rectangle) o;

        if (Double.compare(rectangle.a, a) != 0) return false;
        return Double.compare(rectangle.b, b) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(a);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(b);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }


    @Override
    public void draw(GraphicsContext graphicsContext) {
        graphicsContext.setLineWidth(lineUidth);
        graphicsContext.setStroke(color);
        graphicsContext.strokeRect(center.x-a/2, center.y-b/2, a, b);
    }
    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB (double b) {
        this.b = b;
    }

    @Override
    public void print() {
        System.out.println(toString());
    }

    @Override
    public double getSquare() {
        return a*b;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Rectangle{");
        sb.append("a=").append(a);
        sb.append(", b=").append(b);
        sb.append(", color=").append(color);
        sb.append(", lineUidth=").append(lineUidth);
        sb.append(", center=").append(center);
        sb.append('}');
        return "Rectangle S="+getSquare();
    }
}
