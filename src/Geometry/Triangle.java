package Geometry;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Triangle extends Figure {
    private double base;




    private Triangle( Color color, int lineUidth, Point center) {
        super(FIGURE_TYPE_TRIANGLE, color, lineUidth, center);
    }

    public Triangle( Color color, int lineUidth, Point center, double base) {
        this( color, lineUidth, center);
        this.base = base;
    }

    @Override
    public void draw(GraphicsContext graphicsContext) {
        graphicsContext.setLineWidth(lineUidth);
        graphicsContext.setStroke(color);
        double[] masX={center.x-base/2,center.x,center.x+base/2};
        double[] masY={center.y+base*Math.sqrt(3)/6 ,center.y-base/Math.sqrt(3),center.y+base*Math.sqrt(3)/6};
        graphicsContext.strokePolygon(masX, masY, 3);
    }
    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    @Override
    public void print() {
        System.out.println(toString());
    }

    @Override
    public double getSquare() {
        return base*base/1.73;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Triangle{");
        sb.append("base=").append(base);
        sb.append(", color=").append(color);
        sb.append(", lineUidth=").append(lineUidth);
        sb.append(", center=").append(center);
        sb.append('}');
        return "S Triangle ="+getSquare();
    }
}
