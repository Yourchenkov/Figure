package Geometry;

import javafx.scene.canvas.GraphicsContext;

public class Text implements Drawable{
    public String text;
    public Point point;

    public Text(String text, Point point) {
        this.text = text;
        this.point=point;
    }

    @Override
    public void draw(GraphicsContext graphicsContext) {
        graphicsContext.fillText(text, point.x,point.y);
    }

}
