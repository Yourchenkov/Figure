package Geometry;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Circle extends Figure {
    private double radius;

    private Circle( Color color, int lineUidth, Point center) {
        super(Figure.FIGURE_TYPE_CIRCLE, color, lineUidth, center);
    }

    public Circle(Color color, int lineUidth, Point center, double radius) {
        this( color, lineUidth, center);
        this.radius = radius;
    }

    @Override
    public void draw(GraphicsContext graphicsContext) {
        graphicsContext.setLineWidth(lineUidth);
        graphicsContext.setStroke(color);
        graphicsContext.strokeOval(center.x-radius,center.y-radius , radius*2,radius*2 );
    }
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void print() {
        System.out.println(toString());

    }

    @Override
    public double getSquare() {
        return Math.PI*Math.pow(radius, 2);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Circle{");
        sb.append("radius=").append(radius);
        sb.append(", color=").append(color);
        sb.append(", lineUidth=").append(lineUidth);
        sb.append(", center=").append(center);
        sb.append('}');
        return "Circle S ="+getSquare();
    }
}
