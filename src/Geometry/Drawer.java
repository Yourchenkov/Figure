package Geometry;

import javafx.scene.canvas.GraphicsContext;

import java.util.ArrayList;

public class Drawer<T extends Drawable> {
    private GraphicsContext graphicsContext;

    public Drawer(GraphicsContext graphicsContext) {
        this.graphicsContext = graphicsContext;
    }

    public void repaint(ArrayList<Drawable> aL){
        graphicsContext.clearRect(0,0 ,1024 ,600 );
        for(int i = 0; i < aL.size(); i++){
            if(aL.get(i)!=null)
                aL.get(i).draw(graphicsContext);
        }
    }

}
