package Geometry;
import javafx.scene.paint.Color;

public abstract  class Figure implements Squaraeble,Printable,Drawable {
   private int type;
    protected Color color;
    protected int lineUidth;
    protected Point center;
    public static final int FIGURE_TYPE_RECTANGLE=0;
    public static final int FIGURE_TYPE_TRIANGLE=1;
    public static final int FIGURE_TYPE_CIRCLE=2;
    public static final int FIGURE_TYPE_TEXT=3;
    public static final int FIGURE_TYPE_SPRITE=4;



    public Figure(int type, Color color, int lineUidth, Point center) {
        this.type = type;
        this.color = color;
        this.lineUidth = lineUidth;
        this.center = center;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getLineUidth() {
        return lineUidth;
    }

    public void setLineUidth(int lineUidth) {
        this.lineUidth = lineUidth;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }


    public int getType() {
        return type;
    }

}
