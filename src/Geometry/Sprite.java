package Geometry;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Sprite implements Drawable {
    public Image image;
    public Point point;

    public Sprite(Image image, Point point) {
        this.image = image;
        this.point=point;
    }

    @Override
    public void draw(GraphicsContext graphicsContext) {
        graphicsContext.drawImage(image, point.x, point.y, 60, 60);
    }
}
