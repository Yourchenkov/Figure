package Controller;

import Geometry.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

public class MainScreenController implements Initializable {
    @FXML
    private Canvas canvas;
    private Random rnd;
    private ArrayList<Drawable> drawables;
    public Drawable drawable=null;
    private Drawer drawer;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        drawables=new ArrayList<>();
        drawer=new Drawer<>(canvas.getGraphicsContext2D());
        rnd=new Random(System.currentTimeMillis());
    }
    private Drawable createFigure(Point point){

        switch (rnd.nextInt(5)){
            case Figure.FIGURE_TYPE_RECTANGLE:
            { int a=rnd.nextInt(101);
                a=(a>30)? a : 30;
                int b=rnd.nextInt(101);
                b=(b>30)? b : 30;
                int c=rnd.nextInt(5);
                c=(c!=0)? c : 1;
                drawable=new Rectangle(Color.rgb(rnd.nextInt(200),rnd.nextInt(200),rnd.nextInt(200)),c,point,a,b);
                break;}
            case Figure.FIGURE_TYPE_TRIANGLE:
            {int a=rnd.nextInt(101);
                a=(a>30)? a : 30;
                int c=rnd.nextInt(5);
                c=(c==0)? c : 1;
                drawable=new Triangle(Color.rgb(rnd.nextInt(200),rnd.nextInt(200),rnd.nextInt(200)),c,point,a);
                break;}
            case Figure.FIGURE_TYPE_CIRCLE:
            {int a=rnd.nextInt(51);
                a=(a>30)? a : 30;
                int c=rnd.nextInt(5);
                c=(c==0)? c : 1;
                drawable=new Circle(Color.rgb(rnd.nextInt(200),rnd.nextInt(200),rnd.nextInt(200)),c,point,a);
                //figure.print();
                break;}
            case Figure.FIGURE_TYPE_TEXT:
            {drawable=new Text("x="+point.x+"y="+point.y,point);
                break;}
            case Figure.FIGURE_TYPE_SPRITE:
            {Image image = new Image("Geometry/fruit.png");
                drawable=new Sprite(image,point);
                break;}
            default:
                System.out.println("Unknown Figure Type");
        }
        return drawable;
    }
    private void addFigure(Drawable drawable){
        if(drawable==null)
            return;
        drawables.add(drawable);

    }

    @FXML
    private void canvasClicked(MouseEvent evt) {
        addFigure( createFigure(new Point(evt.getX(),evt.getY())));
        drawer=new Drawer(canvas.getGraphicsContext2D());
        drawer.repaint(drawables);

    }
    private void printOveralSquareable(Squaraeble[] a){
        double S=0.0;
        for (Squaraeble s :
                a) {
            if (s != null){
                S+=s.getSquare();
            }
        }
        System.out.println("SUM="+S);

    }
    void printFigure(Printable[]p){
        for (Printable pr :
                p) {
            if (pr != null) {
                pr.print();
            }
        }
    }
}
